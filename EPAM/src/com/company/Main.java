package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
    System.out.print("Please enter your name: ");
	Scanner input = new Scanner(System.in);
	String name = input.next();
	System.out.println("Hello, " + name + "!");

	System.out.println("How many integers do you want to enter? ");
	int length = input.nextInt();
	int[] numbers = new int[length];
	System.out.print("Enter " + length + " integer numbers: ");
	for (int i = 0; i < length; i++) {
		numbers[i] = input.nextInt();
	}
	int sum = 0;
	int product = 1;
	int i = 0;
	while (i != length) {
		sum += numbers[i];
		product *= numbers[i];
		i++;
	}
	System.out.println("Sum: " + sum);
	System.out.println("Product: " + product);
    }
}
